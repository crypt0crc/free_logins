# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import sys
import os
import requests	
import re

#Define color for stats rating

def Red(skk):print("\033[91m{}\033[00m" .format(skk)) 
def Green(skk):print("\033[92m{}\033[00m" .format(skk))
def Yellow(skk):print("\033[93m{}\033[00m" .format(skk)) 

#Banner

ascii_banner = """
  _                                            _                        
 | |                                          | |                       
 | |__  _   _  __ _ _ __ ___   ___ _ __   ___ | |_   ___ ___  _ __ ___  
 | '_ \| | | |/ _` | '_ ` _ \ / _ \ '_ \ / _ \| __| / __/ _ \| '_ ` _ \ 
 | |_) | |_| | (_| | | | | | |  __/ | | | (_) | |_ | (_| (_) | | | | | |
 |_.__/ \__,_|\__, |_| |_| |_|\___|_| |_|\___/ \__(_)___\___/|_| |_| |_|
               __/ |                                                    
              |___/                                                     

"""
print ascii_banner

#Check params

if (len(sys.argv) < 2):
	print("Introduce all the mandatory parameters!\n")
 	print ("Example : ./bugmenot.py megadede.com")
 	sys.exit()
web = sys.argv[1]
url = 'http://bugmenot.com/view/' + web

#make request
try:
	response = requests.get(url)
	content = response.content
	soup = BeautifulSoup(content, 'lxml')
	accounts = soup.find('article', attrs = {'class': 'account'})
	username = accounts.find('dl').text
	info_soup = soup.find_all('dl')
	##
	print "---------------------------"
	print "**** Current Test Accounts ****"
	print "---------------------------"
	#
	for i in range(len(info_soup)):
			text_info = info_soup[i].text
			username = ('Username: ') + re.findall('Username:(.*)Password',text_info)[0]
			print username

			try:
				other_replace = re.findall('Other:(.*)Stats',text_info)[0]
				password1 = ('Password: ') + re.findall('Password:(.*)Stats',text_info)[0].replace("Other:" + other_replace,"")
				print password1
				other_info1 = ('Other Info: ') + re.findall('Other:(.*)Stats',text_info)[0]
				print other_info1
				if int(re.findall('Stats:  (.*)%',text_info)[0]) >= 80:
					Green(('Stats: ') + re.findall('Stats:  (.*)rate',text_info)[0])
					print ('---------------------')
				if 	int(re.findall('Stats:  (.*)%',text_info)[0]) >= 50 and int(re.findall('Stats:  (.*)%',text_info)[0]) < 80: 
					Yellow(('Stats: ') + re.findall('Stats:  (.*)rate',text_info)[0])
					print ('---------------------')
				if int(re.findall('Stats:  (.*)%',text_info)[0]) < 50:
					Red(('Stats: ') + re.findall('Stats:  (.*)rate',text_info)[0])
					print ('---------------------')

			except IndexError:
				password2 = ('Password: ') + re.findall('Password:(.*)Stats',text_info)[0]
				print password2
				other_info2 = "Other Info : None"
				print other_info2

				if int(re.findall('Stats:  (.*)%',text_info)[0]) >= 80:
					Green(('Stats: ') + re.findall('Stats:  (.*)rate',text_info)[0])
					print ('---------------------')
				if 	int(re.findall('Stats:  (.*)%',text_info)[0]) >= 50 and int(re.findall('Stats:  (.*)%',text_info)[0]) < 80: 
					Yellow(('Stats: ') + re.findall('Stats:  (.*)rate',text_info)[0])
					print ('---------------------')
				if int(re.findall('Stats:  (.*)%',text_info)[0]) < 50:
					Red(('Stats: ') + re.findall('Stats:  (.*)rate',text_info)[0])
					print ('---------------------')

except:
	print "---------------------------"
	print "****------ Fail -------****"
	print "---------------------------"
 	print "Sorry! , Users not found or fatal error!!"
 	sys.exit()